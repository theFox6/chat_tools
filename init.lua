ChatCmdBuilder.new("choose", function(cmd)
	cmd:sub(":list:text", function(_, msg)
		local list = msg:split(";")
		return true, list[math.random(1,#list)]
	end)
end, {
	description = "choose a random element of a \";\" seperated list",
})

local pobability_choice = {
	"most likely","most unlikely",
--	"certainly","certainly not",
	"yes","no",
	"perhaps","maybe",
	"probably","probably not",
}

ChatCmdBuilder.new("8ball", function(cmd)
	cmd:sub(":question:text", function(name, msg)
		minetest.chat_send_all("<" .. name .. "> " .. msg)
		minetest.chat_send_all(pobability_choice[math.random(1,#pobability_choice)])
		return true
	end)
end, {
	description = "says whether a statement will come true",
	privs = {shout=true},
})

ChatCmdBuilder.new("8ball_quiet", function(cmd)
	cmd:sub(":question:text", function(name, msg)
		minetest.log("info",name.." asked: " .. msg)
		return true, pobability_choice[math.random(1,#pobability_choice)]
	end)
end, {
	description = "says whether a statement will come true",
})

ChatCmdBuilder.new("leet", function(cmd)
	cmd:sub(":level:int :msg:text", function(name, level, msg)
		local new_msg = msg
		if level >= 1 then
			new_msg = new_msg:gsub("a","4")
			new_msg = new_msg:gsub("A","4")
			new_msg = new_msg:gsub("e","3")
			new_msg = new_msg:gsub("E","3")
			new_msg = new_msg:gsub("i","1")
			new_msg = new_msg:gsub("I","1")
			new_msg = new_msg:gsub("o","0")
			new_msg = new_msg:gsub("O","0")
		end
		if level >= 2 then
			new_msg = new_msg:gsub("c","(")
			new_msg = new_msg:gsub("C","(")
			new_msg = new_msg:gsub("d","Ð")
			new_msg = new_msg:gsub("D","Ð")
			new_msg = new_msg:gsub("l","£")
			new_msg = new_msg:gsub("L","£")
			new_msg = new_msg:gsub("s","$")
			new_msg = new_msg:gsub("S","$")
			new_msg = new_msg:gsub("u","µ")
			new_msg = new_msg:gsub("U","µ")
			new_msg = new_msg:gsub("y","¥")
			new_msg = new_msg:gsub("Y","¥")
		end
		if level >= 3 then
			new_msg = new_msg:gsub("f","ƒ")
			new_msg = new_msg:gsub("F","ƒ")
			new_msg = new_msg:gsub("g","9")
			new_msg = new_msg:gsub("G","9")
			new_msg = new_msg:gsub("k","|{")
			new_msg = new_msg:gsub("K","|{")
			new_msg = new_msg:gsub("t","7")
			new_msg = new_msg:gsub("T","7")
			new_msg = new_msg:gsub("z","2")
			new_msg = new_msg:gsub("Z","2")
		end
		if level >= 4 then
			new_msg = new_msg:gsub("h","|-|")
			new_msg = new_msg:gsub("H","|-|")
			new_msg = new_msg:gsub("j","/")
			new_msg = new_msg:gsub("J","/")
			new_msg = new_msg:gsub("m","|\\/|")
			new_msg = new_msg:gsub("M","|\\/|")
			new_msg = new_msg:gsub("n","|\\|")
			new_msg = new_msg:gsub("N","|\\|")
			new_msg = new_msg:gsub("v","\\/")
			new_msg = new_msg:gsub("V","\\/")
			new_msg = new_msg:gsub("w","\\/\\/")
			new_msg = new_msg:gsub("W","\\/\\/")
		end
		if level >= 5 then
			new_msg = new_msg:gsub("b","ß")
			new_msg = new_msg:gsub("B","ß")
			new_msg = new_msg:gsub("q","¶")
			new_msg = new_msg:gsub("Q","¶")
			new_msg = new_msg:gsub("r","®")
			new_msg = new_msg:gsub("R","®")
			new_msg = new_msg:gsub("x",")(")
			new_msg = new_msg:gsub("X",")(")
		end
		if level >= 6 then
			new_msg = new_msg:gsub("p","|°")
			new_msg = new_msg:gsub("P","|°")
		end
		minetest.chat_send_all("<" .. name .. "> " .. new_msg)
		return true
	end)
end, {
	description = "converts text to leetspeak with (1-6) severity levels",
	privs = {shout=true},
})
